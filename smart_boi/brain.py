from abc import ABC, abstractmethod

import state_shims
import maneuvers_shim
from kinematics import DesiredKinematics
import logging_utils
import math_utils
import the_professor as prof


class Brain(ABC):
    def __init__(self):
        self.name = self.__class__.__name__
        self.log = logging_utils.make_shared_logger(self.name)

    @abstractmethod
    def compute_desired_kinematics(self, game_info: state_shims.GameInfoShim, car: state_shims.CarShim):
        raise NotImplementedError

class SimpleBrain(Brain):
    """ Brain that probably isn't doing anything remotely intelligent - used for testing new behaviors"""
    def __init__(self):
        Brain.__init__(self)
        # How much time until brain decides to make a new plan
        self.plan_timeout_s = 1.5
        self.last_plan_timestamp_s = 0.0
        self.ball_prediction = None
        self.new_kickoff_detected = False

    def compute_desired_kinematics(self, game_info: state_shims.GameInfoShim, car: state_shims.CarShim):
        if game_info.is_in_kickoff() and not self.new_kickoff_detected:
            self.log.info("Kickoff detected!")
            self.new_kickoff_detected = True
        elif not game_info.is_in_kickoff() and self.new_kickoff_detected:
            self.new_kickoff_detected = False
        self.last_plan_timestamp_s = game_info.get_game_time_s()
        next_possible_hit_time_s = prof.predict_earliest_hit_time(car, game_info)
        if next_possible_hit_time_s is None:
            self.ball_prediction = None
            self.log.info("Couldn't predict a ball location we can reach, driving towards ball...")
            x, y, _ = game_info.get_ball_location_cartesian_uu()
            return DesiredKinematics(location_xyz_uu=(x, y, None), time_s=next_possible_hit_time_s)
        x_ball, y_ball, z_ball = prof.predict_ball_loc_at_time(next_possible_hit_time_s, game_info)
        self.ball_prediction = (x_ball, y_ball, z_ball)
        return DesiredKinematics(location_xyz_uu=(x_ball, y_ball, None))
