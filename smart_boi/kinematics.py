import numpy as np

import math_utils
import consts

_dtype = consts.float_dtype
_zero_vector = np.zeros(3, dtype=_dtype)
# Simple state representations with no outside dependencies
# Useful as both the interface to rlbot/rlutil states wrapped in shims
# and for creating dummy states for planning/prediction

# TODO: Move shared members for all physics-based state to a base class with a fixed interface
class Ball:
    def __init__(self, location_xyz_uu, orientation_rpy_rads):
        self._position = np.array(location_xyz_uu, dtype=_dtype)
        self._orientation = np.array(orientation_rpy_rads, dtype=_dtype)

class Kinematics:
    # TODO: Add rest of physics fields
    def __init__(self, location_xyz_uu,
                 rotation_rpy_rads,
                 velocity_xyz=_zero_vector,
                 acceleration_xyz=_zero_vector):
        self._position = np.array(location_xyz_uu, dtype=_dtype)
        self._orientation = np.array(rotation_rpy_rads, dtype=_dtype)
        self._velocity = np.array(velocity_xyz, dtype=_dtype)
        self._acceleration = np.array(acceleration_xyz, dtype=_dtype)

    # TODO: Fix shims to update all appropriate fields
    def update_fields(self, location, rotation, velocity, acceleration=_zero_vector):
        self._position = np.array(location, dtype=_dtype)
        self._orientation = np.array(rotation, dtype=_dtype)
        self._velocity = np.array(velocity, dtype=_dtype)
        self._acceleration = np.array(acceleration, dtype=_dtype)

    def __len__(self):
        # TODO: Remember to modify when adding more fields
        return 4

    def __iter__(self):
        # Allows us to unpack the values in a known order
        yield self._position
        yield self._orientation
        yield self._velocity
        yield self._acceleration

    def set_acceleration_xyz_uu_s2(self, acceleration):
        if len(acceleration) != 3:
            raise ValueError("Attempted to set acceleration vector with %s" % str(acceleration))
        self._acceleration = np.array(acceleration, dtype=_dtype)

    # TODO: Add option to include acceleration due to gravity
    def set_acceleration_scalar_uu_s2(self, acceleration):
        self.set_acceleration_xyz_uu_s2(acceleration * self.get_forward_vector_xyz_uu())

    def get_forward_vector_xyz_uu(self):
        _, pitch, yaw = self._orientation
        return np.array(math_utils.euler_angles_to_forward_vector(pitch, yaw), dtype=_dtype)

    def get_velocity_vector_xyz_uu_s(self):
        return self._velocity

    def get_roll_pitch_yaw_rads(self):
        return self._orientation

    def get_location_xyz_uu(self):
        return self._position

    def get_speed_uu_s(self):
        return np.linalg.norm(self._velocity)


class DesiredKinematics(Kinematics):
    def __init__(self,
                 time_s=None,
                 location_xyz_uu=None,
                 rotation_rpy_rads=None,
                 velocity_xyz=None,
                 acceleration_xyz=None):
        Kinematics.__init__(self, location_xyz_uu, rotation_rpy_rads, velocity_xyz, acceleration_xyz)
        self.time_s = time_s

    def matches_within_tolerance(self, state: Kinematics):
        #TODO: Implement
        raise NotImplementedError

    # TODO: These comparisons are broken and need to be fixed (can't compare arrays)
    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        else:
            return False

    def __ne__(self, other):
        return not self == other

    def has_position_xy(self):
        """ Checks whether this object has a target location defined for x and y """
        if self._position is not None:
            x, y, _ = self._position
            return x is not None and y is not None
        else:
            return False
