import datetime

import maneuvers_shim
import brain
import pilot
import logging_utils
import state_shims

from rlbot.agents.base_agent import BaseAgent, SimpleControllerState
from rlbot.utils.structures.game_data_struct import GameTickPacket


class SmartBoi(BaseAgent):
    def __init__(self, *args, **kwargs):
        BaseAgent.__init__(self, *args, **kwargs)
        time_string = datetime.datetime.now().isoformat()
        # Store name for this log folder in case member components need it
        self.log_folder_name = time_string.replace("-", "").replace(":", "-").replace(".", "_")
        self.log = logging_utils.set_up_root_loggers(self.log_folder_name)
        self.log.info("Initializing agent...")
        # Set these as reference the the classes that will get constructed later so the IDE has some context
        self.game_info = state_shims.GameInfoShim()
        self.cars = [state_shims.CarShim]
        self.pilot = pilot.Pilot()
        self.brain = brain.SimpleBrain()
        # State tracking can't be initialized until game tick packet starts feeding state
        # This flag indicates whether we've initialized all our critical components with an initial state
        self.components_are_initialized = False
        self.has_kickoff_plan = False

    def initialize_agent(self):
        self.game_info.initialize_info(self.index, self.team, self.get_field_info())
        self.log.info("initialize_agent finished - still need to initialize agent's members with game tick info")

    @staticmethod
    def _construct_car_shims(game_packet, game_info):
        if not game_packet.game_cars:
            raise RuntimeError("Game packet is missing its game_cars array.\n%s" % game_packet.__dir__)
        elif not game_info.cars:
            raise RuntimeError("Game info is missing its cars array.")
        return [state_shims.CarShim(rlbot, rlutil) for rlbot, rlutil in zip(game_packet.game_cars, game_info.cars)]

    def _initialize_components(self, game_packet: GameTickPacket, game_info: state_shims.GameInfoShim):
        self.log.info("Constructing car shims [%d num_cars | %d cars in game_cars | %d in rlutil.cars]"
                      % (game_packet.num_cars, len(game_packet.game_cars), len(game_info.rlutil.cars)))
        # TODO: Figure out how to get types for these members recognized by the IDE
        self.cars = self._construct_car_shims(game_packet, game_info.rlutil)
        return True

    def _update_states(self, game_packet: GameTickPacket) -> bool:
        # NOTE: game info update MUST come first to update the state of all sub-modules' references
        self.game_info.update(self, game_packet)
        # Check that everything is initialized first...
        if not self.components_are_initialized:
            if game_packet.num_cars == 0:
                return False
            else:
                self.components_are_initialized = self._initialize_components(game_packet, self.game_info)
                if self.components_are_initialized:
                    self.log.info("=======  COMPONENTS INITIALIZED =======")
                else:
                    self.log.exception("Failed to fully initialize agent's components.")
                    raise RuntimeError("Failed to initialize components with first valid game packet.")

        # Call update functions for other members that need manual updates
        for car_shim, game_car in zip(self.cars, game_packet.game_cars):
            car_shim.update(game_car)
        return True

    def can_move(self, info):
        return info.is_round_active and not info.is_match_ended

    def get_agent_car(self):
        if self.components_are_initialized:
            return self.game_info.find_own_car(self.cars)

    def get_output(self, packet: GameTickPacket) -> SimpleControllerState:
        self.log.debug("Getting output")
        # If state update failed, can't compute a new controller state
        update_succeeded = self._update_states(packet)
        if not update_succeeded:
            self.log.debug("Still waiting for first useful packet...")
            return SimpleControllerState()
        if not self.can_move(packet.game_info):
            self.log.debug("Waiting to be able to move...")
            return SimpleControllerState()

        car = self.get_agent_car()
        target = self.brain.compute_desired_kinematics(self.game_info, car)
        return self.pilot.get_next_controller_state(car, target)
