import numpy as np

import kinematics
import consts

class Region:
    def __init__(self):
        pass

class SphericalRegion(Region):
    def __init__(self, center_xyz_uu, radius_uu):
        Region.__init__(self)
        self.center_xyz_uu = center_xyz_uu
        self.radius_uu = radius_uu

    def is_point_inside(self, point_xyz_uu):
        distance = compute_distance_between_points(point_xyz_uu, self.center_xyz_uu)
        return distance <= self.radius_uu

def compute_distance_between_states(state1: kinematics.Kinematics, state2: kinematics.Kinematics):
    p1 = state1.get_location_xyz_uu()
    p2 = state2.get_location_xyz_uu()
    return compute_distance_between_points(p1, p2)

def compute_distance_between_points(p1, p2):
    return np.linalg.norm(p1 - p2)

def compute_inner_angle_unit_vectors_rads(v1_u, v2_u):
    """ Computes angle between two vectors ASSUMING they are normalized already """
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))

def compute_inner_angle_vectors_rads(v1, v2):
    """ Computes the angle between two vectors after normalizing them """
    v1_u, v2_u = (normalize_vector(v) for v in (v1, v2))
    return compute_inner_angle_unit_vectors_rads(v1_u, v2_u)

def compute_heading_correction_rads(current_location_xy, target_location_xy, current_yaw_world_rads):
    dx, dy = [t - c for t, c in zip(target_location_xy, current_location_xy)]
    # NOTE: x is the "forward" direction and the axes are left-handed
    target_yaw_world_rads = np.arctan2(dy, dx)
    heading_correction_rads = target_yaw_world_rads - current_yaw_world_rads
    return fix_wraparound_rad(heading_correction_rads)

def euler_angles_to_forward_vector(pitch, yaw):
    sp = np.sin(pitch)
    cp = np.cos(pitch)
    sy = np.sin(yaw)
    cy = np.cos(yaw)
    return (cp * cy, cp * sy, sp)

def fix_wraparound_rad(rotation_rad):
    """ Restricts a radian value to plus/minus one half rotation """
    one_rotation = 2.0 * np.pi
    while rotation_rad > np.pi:
        rotation_rad -= one_rotation
    while rotation_rad < -np.pi:
        rotation_rad += one_rotation
    return rotation_rad

def compute_vehicle_acceleration_xyz(vehicle_state: kinematics.Kinematics, throttle):
    forward_vector = vehicle_state.get_forward_vector_xyz_uu()
    forward_velocity = np.dot(vehicle_state.get_velocity_vector_xyz_uu_s(), forward_vector)
    # Don't divide by zero
    if np.abs(forward_velocity) < consts.eps:
        if np.abs(throttle) < consts.throttle_eps:
            return consts.zero_vector
        else:
            return consts.compute_throttle_acceleration_uu_s2(0, throttle) * forward_vector
    else:
        return consts.compute_throttle_acceleration_uu_s2(forward_velocity, throttle) * forward_vector

def integrate_kinematics(initial_state: kinematics.Kinematics, time_interval_s):
    """
    Simple integration of kinematic state with respect to a time interval
    # NOTE: Assumes acceleration is constant over this interval
    # TODO: Integrate rotational components
    """
    p_0, r_0, v_0, a_0 = initial_state
    v_delta = a_0 * time_interval_s
    p = p_0 + (v_0 + 0.5 * v_delta) * time_interval_s
    v = v_0 + v_delta
    return kinematics.Kinematics(p, r_0, v, a_0)

def inscribe_sphere(point1_xyz, point2_xyz):
    """
    Create a spherical region inscribed between two cartesian coordinates
    :param point1_xyz: first coordinate
    :param point2_xyz: second coordinate
    :return: SphericalRegion
    """
    mid_point = np.mean([point1_xyz, point2_xyz], 0)
    radius = np.linalg.norm(point1_xyz - point2_xyz) / 2.0
    return SphericalRegion(mid_point, radius)

def location_to_numpy(loc):
    return np.array([loc.x, loc.y, loc.z], dtype=np.float32)

def normalize_vector(v):
    norm = np.linalg.norm(v)
    if norm > consts.eps:
        return v / norm
    else:
        return consts.zero_vector

def rotation_to_numpy(rot):
    return np.array([rot.pitch, rot.yaw, rot.roll], dtype=np.float32)


