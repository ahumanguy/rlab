import logging

import agent
import debug_shim
from debug_shim import Colors

class DebugBoi(agent.SmartBoi):
    def __init__(self, *args, **kwargs):
        agent.SmartBoi.__init__(self, *args, **kwargs)
        self.log.setLevel(logging.DEBUG)
        self.debug = debug_shim.DebugDrawer
        self.log.info("Debug boy constructed!")
        self.last_pilot_status = ""
        self.last_ball_prediction = None

    def initialize_agent(self):
        super().initialize_agent()
        debug_shim.initialize_debug_drawer(self.renderer)
        self.debug = debug_shim.get_debug_drawer()
        self.log.info("Debug boy initialized!")

    def _draw_pilot_status(self):
        pilot_status = self.pilot.get_status_string()
        if self.last_pilot_status[:-20] != pilot_status[:-20]:
            self.last_pilot_status = pilot_status
            if "finished" in pilot_status.lower():
                color = Colors.lime
            elif "pilot" in pilot_status.lower():
                color = Colors.yellow
            else:
                color = Colors.white
            self.debug.draw_console_text(pilot_status, color=color)
            self.debug.draw_text(pilot_status, coords=self.get_agent_car().get_location_xyz_uu())

    def _draw_brain_status(self):
        ball = self.brain.ball_prediction
        if ball is not None:
            self.debug.draw_ball(ball, Colors.yellow)
            if self.last_ball_prediction is None or ball != self.last_ball_prediction:
                #self.debug.draw_console_text("New ball prediction: %s" % str(ball), color=Colors.yellow)
                self.last_ball_prediction = ball
        else:
            self.last_ball_prediction = None

    def _draw_game_state_info(self):
        ball_pos = self.game_info.get_ball_location_cartesian_uu()
        self.debug.draw_text("Ball Position: %s" % str(ball_pos))
        self.debug.draw_ball(ball_pos, Colors.green)
        ball_predictions = self.game_info.get_ball_prediction_points()
        if ball_predictions is not None:
            self.debug.draw_polyline(ball_predictions, Colors.yellow)

    def get_output(self, packet):
        # Call underlying get_output first to make sure all state is current
        output = super().get_output(packet)
        if self.components_are_initialized:
            self._draw_pilot_status()
            self._draw_brain_status()
            self._draw_game_state_info()
            self.debug.render_frame()

        return output