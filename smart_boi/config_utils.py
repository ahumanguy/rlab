import os
import configparser

_config_name = "smart_boi_modules.cfg"

def load_config(section=None):
    this_dir = os.path.dirname(os.path.realpath(__file__))
    config_file = os.path.join(this_dir, _config_name)
    parser = configparser.RawConfigParser()
    with open(config_file, 'r') as f:
        parser.read_file(f)
    if section:
        return parser[section]
    else:
        return parser
