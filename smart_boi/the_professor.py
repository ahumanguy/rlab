import collections
import numpy as np

import state_shims
import kinematics
import consts
import math_utils

def compute_reachable_regions(car: kinematics.Kinematics, time_window_s):
    """
    Attempts to compute reasonable bounds for what is reachable in a given time window
    Assumes acceleration is applied only along its initial velocity vector and that turning is instantaneous
    NOTE: Assuming no use of boost and car is on ground as well, for now
    """
    time_step = consts.physics_slice_time_s
    if time_window_s < time_step:
        raise ValueError("Time window ({:.3f} seconds) but be at least one physics step ({:.3f} seconds)."\
            .format(time_window_s, time_step))
    elapsed_time_s = time_step
    throttle_values = [-1.0, 1.0]
    kinematics_forward = kinematics.Kinematics(*car)
    kinematics_reverse = kinematics.Kinematics(*car)
    for k, t in zip([kinematics_reverse, kinematics_forward], throttle_values):
        initial_acceleration_vector = math_utils.compute_vehicle_acceleration_xyz(k, t)
        k.set_acceleration_xyz_uu_s2(initial_acceleration_vector)
    # Furthest kinematic state reachable by using throttle = -1
    while elapsed_time_s <= time_window_s:
        # XXX: Because acceleration changes as a function of velocity, these are not fully accurate integrations
        for k, t in zip([kinematics_reverse, kinematics_forward], throttle_values):
            k.update_fields(*math_utils.integrate_kinematics(k, time_step))
            new_acceleration_vector = math_utils.compute_vehicle_acceleration_xyz(k, t)
            k.set_acceleration_xyz_uu_s2(new_acceleration_vector)

        farthest_forward = kinematics_forward.get_location_xyz_uu()
        farthest_back = kinematics_reverse.get_location_xyz_uu()
        yield math_utils.inscribe_sphere(farthest_back, farthest_forward)
        elapsed_time_s += time_step

# TODO: Remove need for the shims here and just use simple state for everything?
# ... may not be feasible because the ball_predictions are relatively complex objects
def predict_earliest_hit_time(car: kinematics.Kinematics, game_info: state_shims.GameInfoShim):
    current_time = game_info.get_game_time_s()
    time_window = game_info.ball_prediction_period_s
    for region, ball in zip(compute_reachable_regions(car, time_window), game_info.get_ball_prediction_slices()):
        current_time += consts.physics_slice_time_s
        ball_center = state_shims.PhysicsShim.read_location_xyz_uu(ball.physics)
        height_above_ground = consts.ball_radius_uu + consts.octane_com_height_uu * 2.0
        # TODO: Remove the restriction to ground shots
        if region.is_point_inside(ball_center) and ball_center[2] <= height_above_ground:
            return current_time
    return None

def predict_ball_loc_at_time(game_time_s: float, game_info: state_shims.GameInfoShim):
    elapsed_time_s = game_time_s - game_info.get_game_time_s()
    if not 0.0 <= elapsed_time_s <= game_info.ball_prediction_period_s:
        raise ValueError("Cannot predict ball location for time %f because it's out of range" % game_time_s)
    return predict_ball_loc_after_elapsed_s(elapsed_time_s, game_info)

def predict_ball_loc_after_elapsed_s(elapsed_time_s: float, game_info: state_shims.GameInfoShim):
    return game_info.get_ball_location_cartesian_uu(time_elapsed_s=elapsed_time_s)

# TODO: get_states_for_directional_hit (computes vehicle states that can perform the hit requested)
