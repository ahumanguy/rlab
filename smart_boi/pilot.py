import numpy as np

# TODO: Compartmentalize dependencies properly and remove wildcard import
from maneuvers_shim import *
import kinematics

import logging

_logging_identifier = "pilot"

# Static value for the presumed time delta between simulation steps
# NOTE: Right now looks like sim is expected to run at 60 Hz
_dt = state_shims.GameInfo.GameInfo.DT

# Some constants taken from RLUtilities
_max_throttle_speed = consts.max_throttle_speed_uu_s
_max_boost_speed = consts.max_boost_speed_uu_s

_target_difference_threshold = 100.0

class Pilot:
    """
    Planner and executor which attempts to achieve the state proposed by the agent
    """
    def __init__(self):
        self.maneuver = None
        self.status_prefix = "[{}]".format("pilot")
        log_cfg = logging_utils.load_log_config()
        self.log = logging.getLogger("%s.%s" % (logging_utils.get_shared_logger_name(log_cfg), _logging_identifier))
        self.data_log = logging_utils.make_data_logger(_logging_identifier)
        self.log.info("Pilot initialized.")
        self.data_log.info("Pilot black box started.")
        self.turn_threshold = np.pi / 4.0

    def _should_turn(self, car, desired_kinematics):
        car_xy  = car.get_location_xyz_uu()[:2]
        target_xy = desired_kinematics.get_location_xyz_uu()[:2]
        _, _, yaw_car = car.get_roll_pitch_yaw_rads()
        correction = np.abs(math_utils.compute_heading_correction_rads(car_xy, target_xy, yaw_car))
        distance = math_utils.compute_distance_between_states(desired_kinematics, car)
        return correction > self.turn_threshold or distance < _target_difference_threshold

    def _update_maneuver(self, car, desired_kinematics):
        # TODO: Refine this to analyze the desired_kinematics and pick the best maneuver to get there
        x, y, _ = desired_kinematics.get_location_xyz_uu()
        if self.maneuver and not self.maneuver.is_finished():
            self.log.debug("Checking current maneuver...")
            distance = math_utils.compute_distance_between_states(desired_kinematics, self.maneuver.target)
            if distance < _target_difference_threshold:
                self.log.debug("Difference only %f, updating current maneuver" % distance)
                self.maneuver.update_target(desired_kinematics)
                return

            self.log.debug("Difference is %f, setting a new maneuver" % distance)

        if self._should_turn(car, desired_kinematics):
            self.maneuver = TurnTowardsXYLoc(car, desired_kinematics)
        else:
            self.maneuver = DriveToLoc(car, desired_kinematics)
        self.log.info("Set new maneuver: %s" % self.maneuver.name)

    def get_status_string(self):
        """ Provides a human-readable summary of what pilot is doing at the time this is called """
        if self.maneuver:
            if self.maneuver.is_finished():
                return "{} finished {}!".format(self.status_prefix, self.maneuver.name)
            else:
                return self.maneuver.status_string
        else:
            return "{} no maneuver assigned!".format(self.status_prefix)

    def get_next_controller_state(self, car: state_shims.CarShim, target: kinematics.DesiredKinematics):
        self._update_maneuver(car, target)
        # TODO: If maneuver fails viability check something should happen (here or in brain probably)
        if self.maneuver and not self.maneuver.is_finished():
            self.log.debug("Getting controls from %s" % self.maneuver.name)
            controls = self.maneuver.do_step(_dt)
            if not self.maneuver.is_managing_boost:
                # TODO: Do I want logic here?
                pass
            return controls
        else:
            self.log.error("Don't have a maneuver to execute!")
            return SimpleControllerState()
