import os
import logging

import config_utils

# TODO: This doesn't work! Keep it to one agent until I can fix this
_num_agents = 0

def load_log_config():
    return config_utils.load_config("Logging")


# TODO: Need to devise a way to infer which agent's submodules are calling for these names
def get_shared_logger_name(log_config):
    return log_config["shared_logger_name"]


def get_data_log_name(log_config):
    return log_config["shared_data_logger_name"]


def make_shared_logger(log_subgroup):
    log_config = load_log_config()
    parent_log_name = get_shared_logger_name(log_config)
    return logging.getLogger("%s.%s" % (parent_log_name, log_subgroup))


# NOTE: May want to create multiple data loggers with different roots (to stream to different output files)
#       Keep this in mind when modifying log helpers
def make_data_logger(log_subgroup):
    log_config = load_log_config()
    parent_log_name = get_data_log_name(log_config)
    return logging.getLogger("%s.%s" % (parent_log_name, log_subgroup))


def set_up_root_loggers(log_name):
    global _num_agents
    this_dir = os.path.dirname(os.path.realpath(__file__))

    config = load_log_config()
    log_dir_name = "Logs"
    if config.getboolean("use_dated_log"):
        log_dir = os.path.join(this_dir, log_dir_name, log_name)
        os.makedirs(log_dir)
    else:
        log_dir = os.path.join(this_dir, log_dir_name)

    # TODO: Adjust this naming after determining how to correctly assign log names
    log_file = os.path.join(log_dir, "%s%s.log" % (config['agent_log_name'], str(_num_agents)))
    handler = logging.FileHandler(log_file, mode='w')
    handler.setLevel(int(config["agent_log_level"]))
    formatter = logging.Formatter(config["agent_log_format"])
    handler.setFormatter(formatter)
    logger = logging.getLogger(config["shared_logger_name"])
    logger.addHandler(handler)

    # Create a log with a static location that only logs info and above for monitoring purposes
    console_file = os.path.join(this_dir, log_dir_name, "%s.log" % config['console_log_name'])
    console_handler = logging.FileHandler(console_file, mode='w+')
    console_handler.setLevel(logging.INFO)
    console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)

    # This log doesn't get used here - but configure it here anyway
    data_logger = logging.getLogger(config["shared_data_logger_name"])
    data_log_file = os.path.join(log_dir, "%s.log" % config["data_log_name"])
    data_handler = logging.FileHandler(data_log_file, mode='w')
    data_handler.setLevel(int(config["data_log_level"]))
    data_handler.setFormatter(logging.Formatter(config['data_log_format']))
    data_logger.addHandler(data_handler)

    return logging.getLogger("%s.%s" % (config["shared_logger_name"], "base"))
