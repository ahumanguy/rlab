from abc import ABC, abstractmethod
import numpy as np

import math_utils
import game_clock
import consts
import logging_utils
import state_shims
import kinematics

from rlbot.agents.base_agent import SimpleControllerState

# Maneuvers are not easily shimmed because they have no base class, so they are pseudo-shimmed here
from RLUtilities import Maneuvers

# Static value for the presumed time delta between simulation steps
# NOTE: Right now looks like sim is expected to run at 60 Hz
_dt = state_shims.GameInfo.GameInfo.DT

# Some constants taken from RLUtilities
_max_throttle_speed = consts.max_throttle_speed_uu_s
_max_boost_speed = consts.max_boost_speed_uu_s

class ExtendedManeuver(ABC):
    """
    Class for extending the Maneuver classes with a fixed interface and potentially additional functionality that
    matches custom maneuvers defined here
    NOTE: Pose and physics information is assumed to be in world frame unless otherwise stated
    """
    # >>>>> TODO: Implement "expected_outcome" using simple_states
    def __init__(self, active_car: state_shims.CarShim, target_kinematics: kinematics.DesiredKinematics):
        self.finished = False
        self.name = self.__class__.__name__
        self.status_prefix = "[{}]".format(self.name)
        self.status_string = "{} initialized.".format(self.status_prefix)
        self.data_log = logging_utils.make_data_logger(self.name)
        # NOTE: Maneuver uses self.car - don't use that same name here
        self.this_car = active_car
        self.target = target_kinematics
        # NOTE: This may be overwritten by Maneuver constructor
        self.controls = SimpleControllerState()
        self.data_log.debug(self.status_string)
        # Whether or not this maneuver has logic to manage boost
        self.is_managing_boost = False
        # By default, THIS is the maneuver, but in advanced cases we'll use RLUtil or select from a list based on state
        self.current_maneuver = self
        if not self._target_has_required_fields(self.target):
            raise ValueError("%s wasn't passed relevant values from %s" % (self.name, self.target))

    @abstractmethod
    def _target_has_required_fields(self, target: kinematics.DesiredKinematics):
        """
        There is no guarantee each field in a DesiredKinematics object is populated, but each Maneuver will
        need certain fields populated in order to execute. This checks to ensure the appropriate fields have data.
        :param target: a potential kinematic state this maneuver will attempt to achieve
        :return: bool
        """
        raise NotImplementedError


    def do_step(self, dt):
        # In the simple case, there is a single maneuver being managed by this class
        self.current_maneuver.step(dt)
        self.finished = self.current_maneuver.finished
        self.update_status_string()
        self.data_log.debug(self.status_string)
        return self.current_maneuver.controls

    def step(self, dt):
        # This only needs to implemented in cases where the maneuver is not strictly wrapping a RLUtil maneuver
        raise NotImplementedError

    def is_viable(self):
        return True

    def is_finished(self):
        return self.finished or not self.is_viable()

    def log_complete(self):
        self.data_log.info(">>>>>MANEUVER COMPLETE<<<<<<")

    def log_controls(self):
        t, s, h = self.controls.throttle, self.controls.steer, self.controls.handbrake
        self.data_log.debug("T: %f, S: %f, H: %f" % (t, s, h))

    @abstractmethod
    def update_status_string(self):
        raise NotImplementedError

    def update_target(self, new_target: kinematics.DesiredKinematics):
        if not self._target_has_required_fields(new_target):
            raise ValueError("%s can't update target with values from %s" % (self.name, new_target))
        else:
            self.data_log.debug("[%s] Updating target... " % self.name)
            self.target = new_target


# TODO: Modify init functions to always take the desired kinematics as the target
class TurnTowardsXYLoc(ExtendedManeuver):
    def __init__(self, active_car, target_kinematics):
        ExtendedManeuver.__init__(self, active_car, target_kinematics)
        # TODO: Tune value - model as function of delta yaw, distance to target, current speed, and angular velocity
        self.brake_threshold_rads = np.radians(45)
        self.success_threshold_rads = np.radians(10)
        self.is_managing_boost = True
        self.correction = 0

    def _should_handbrake(self, rads_to_turn):
        return np.abs(rads_to_turn) > self.brake_threshold_rads

    def _target_has_required_fields(self, target):
        return target.has_position_xy()

    def step(self, dt):
        # TODO: RLab-13 PID-controlled counter-steer to zero out angular momentum as we approach target vector
        target_xy = self.target.get_location_xyz_uu()[:2]
        x, y, _ = self.this_car.get_location_xyz_uu()
        _, _, yaw = self.this_car.get_roll_pitch_yaw_rads()
        self.correction = math_utils.compute_heading_correction_rads((x, y), target_xy, yaw)
        self.data_log.info(self.status_string)
        self.controls.throttle = 1
        self.controls.handbrake = int(self._should_handbrake(self.correction))
        # Either boost or handbrake
        self.controls.boost = 1 - self.controls.handbrake
        self.controls.steer = np.sign(self.correction)
        if abs(self.correction) < self.success_threshold_rads:
            self.log_complete()
            self.finished = True
        return self.is_finished()

    def update_status_string(self):
        target_xy = self.target.get_location_xyz_uu()[:2]
        target_str = "({:.1f},{:.1f})".format(*target_xy)
        self.status_string = "{} turning to {} | {:.2f} degrees to go".format(self.status_prefix,
                                                                              target_str,
                                                                              float(np.degrees(self.correction)))

class DriveToLoc(ExtendedManeuver):
    def __init__(self, active_car: state_shims.CarShim, target_kinematics):
        ExtendedManeuver.__init__(self, active_car, target_kinematics)
        self.current_maneuver = self._construct_maneuver(active_car, target_kinematics)
        self.is_managing_boost = True

    def _construct_maneuver(self, car, target_kinematics):
        target_location_xy = target_kinematics.get_location_xyz_uu()[:2]
        if target_kinematics.time_s is not None:
            distance = math_utils.compute_distance_between_states(car, target_kinematics)
            time_delta_s = target_kinematics.time_s - game_clock.get_time_s()
            if time_delta_s <= consts.eps:
                self.data_log.error("Target kinematics has an impossible time window: %f" % time_delta_s)
                target_speed = consts.max_throttle_speed_uu_s
            else:
                target_speed = distance / time_delta_s
        else:
            target_speed = consts.max_boost_speed_uu_s

        return Maneuvers.Drive(car._rlutil, Maneuvers.vec3(*target_location_xy, 0), target_speed)

    def _target_has_required_fields(self, target):
        return target.has_position_xy()

    def update_status_string(self):
        target_xy = self.target.get_location_xyz_uu()[:2]
        target_str = "({:.1f}, {:.1f})".format(*target_xy)
        self.status_string = "{} driving to {}".format(self.status_prefix, target_str)

    def update_target(self, new_target):
        super().update_target(new_target)
        self.current_maneuver = self._construct_maneuver(self.this_car, new_target)
