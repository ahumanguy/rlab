import numpy as np

eps = np.finfo(float).eps
# Minimum value throttle can be set to before being treated as zero
throttle_eps = 0.01
float_dtype = np.float64
sqrt2 = np.sqrt(2.0)

ball_radius_uu = 92.75
octane_com_height_uu = 16.5

gravity_uu_s2 = 650.0
max_throttle_speed_uu_s = 1410.0
max_throttle_accel_uu_s2 = 1600.0
max_boost_speed_uu_s = 2300.0
car_mass = 180.0                # arbitrary units
accel_boost_uu_s2 = max_throttle_accel_uu_s2 + 991.0 + 2.0/3.0
accel_brake_uu_s2 = 3500.0
accel_coast_uu_s2 = 525.0
# MISSING: Negative acceleration when going over max throttle speed without boost
ball_restitution = 0.6          # unit-less
jump_hold_duration_s = 0.2
# TODO: Verify units
max_angular_accel_yaw_rads_s2 = 9.11
max_angular_accel_pitch_rads_s2 = 12.46
max_angular_accel_roll_rads_s2 = 38.34

# Approximated values
jump_initial_vel_uu_s = 300.0
# NOTE: Not including gravity
accel_jumping_uu_s2 = 1400.0

physics_slice_time_s = 1.0/60.0

# Commonly used vectors
zero_vector = np.array([0.0, 0.0, 0.0], dtype=float_dtype)
forward_vector = np.array([1.0, 0.0, 0.0], dtype=float_dtype)
backward_vector = np.array([-1.0, 0.0, 0.0], dtype=float_dtype)
up_vector = np.array([0.0, 0.0, 1.0], dtype=float_dtype)
down_vector = np.array([0.0, 0.0, -1.0], dtype=float_dtype)
# NOTE: Left-handed coordinates
right_vector = np.array([0.0, 1.0, 0.0], dtype=float_dtype)
left_vector = np.array([0.0, -1.0, 0.0], dtype=float_dtype)

def compute_throttle_acceleration_uu_s2(forward_velocity, throttle=1.0):
    """
    Computes acceleration from applied throttle as a function of speed
    """
    if not 1 >= throttle >= -1:
        raise ValueError("compute_throttle expects throttle to be between -1.0 and 1.0 inclusive")
    if -throttle_eps < throttle < throttle_eps:
        return -accel_coast_uu_s2
    else:
        # Only return braking acceleration if signs are OPPOSITE
        if np.sign(forward_velocity) == -1 * np.sign(throttle):
            return accel_brake_uu_s2 * throttle
        else:
            speed = np.abs(forward_velocity)
            if 0.0 <= speed < 1400:
                return ((-36.0/35.0) * speed + 1600) * throttle
            elif 1400 <= speed < 1410 - eps:
                return (-16 * speed + 22560) * throttle
            else:
                # Throttle provides no acceleration if speed is at or above non-boost max
                return 0

# These functions are static and rely on these const vals, so I keep them here
def compute_curvature(v):
    if 0.0 <= v < 500.0:
        return 0.006900 - 5.84e-6 * v
    elif 500.0 <= v < 1000.0:
        return 0.005610 - 3.26e-6 * v
    elif 1000.0 <= v < 1500.0:
        return 0.004300 - 1.95e-6 * v
    elif 1500.0 <= v < 1750.0:
        return 0.003025 - 1.10e-6 * v
    elif 1750.0 <= v < 2500.0:
        return 0.001800 - 0.40e-6 * v
    else:
        return 0.0

def compute_turn_radius(velocity):
    if velocity == 0.0:
        return 0.0
    else:
        return 1.0 / compute_curvature(velocity)
