import queue
from collections import namedtuple
import numpy as np

import config_utils
import logging_utils
import state_shims
import consts

# This is violating my "no rlbot imports outside of shims" rule - but the debugging interface will be big enough
# that it needs its own module, even though this is still mostly a shim
from rlbot.utils.rendering import rendering_manager

# Keep one static instance of the drawing class in this module to simplify the config_utils api
_instance = None
# Number of lines to allow in console print
_console_length_limit = 20
# Number of frames to allow a console output to persist (assuming 60 frames/s)
_console_age_limit = 60.0 * 3.0

_text_height = 15
_ball_text_scale = 1

# Generically defined tuple for declaring draw actions to put into DebugDrawer's buffer
DrawAction = namedtuple('DrawAction', ['draw_fn', 'args', 'kwargs'])

class Colors:
    """ Simple class to make typing colors in my IDE easier and prettier """
    # TODO: there is probably a cleaner way to fetch these colors from the renderer without using get_color below
    black = "black"
    white = "white"
    gray = "gray"
    blue = "blue"
    red = "red"
    green = "green"
    lime = "lime"
    yellow = "yellow"
    orange = "orange"
    cyan = "cyan"
    pink = "pink"
    purple = "purple"

class ConsoleLine:
    def __init__(self, text, color=Colors.white):
        self.text = text
        self.frame_age = 0
        self.color = color

def load_debug_config():
    return config_utils.load_config("Debug")


def get_logger_name(debug_config):
    return debug_config["logging_name"]


def initialize_debug_drawer(renderer):
    global _instance
    if _instance:
        raise RuntimeError("initialize_debug_drawer can not be called again - already initialized")
    else:
        _instance = DebugDrawer(renderer)


def get_debug_drawer():
    if not _instance:
        raise RuntimeError("Attempted to fetch the debug drawer before it was initialized")
    else:
        return _instance

# TODO: Devise a thread-safe mechanism to enqueue all the rendering actions that need to be taken for a given tick

class Line3D:
    def __init__(self, start_xyz, end_xyz, color=Colors.white):
        self.start = self.fix_type(start_xyz)
        self.end = self.fix_type(end_xyz)
        self.color = _instance.get_color(color)

    @staticmethod
    def fix_type(loc_xyz):
        return loc_xyz if type(loc_xyz) is state_shims.rlbot_structs.Vector3 else state_shims.xyz_to_rlbot_vector(*loc_xyz)


#TODO: Make a StreamHandler object that prints to screen
class DebugDrawer:

    def __init__(self, renderer: rendering_manager):
        self.renderer = renderer
        self.draw_actions = queue.Queue()
        debug_config = load_debug_config()
        self.log = logging_utils.make_shared_logger(get_logger_name(debug_config))
        self.line_drawing_enabled = debug_config.getboolean("render_lines")
        self.text_drawing_enabled = debug_config.getboolean("render_text")
        self.console_buffer = [ConsoleLine("")] * _console_length_limit
        self.next_console_ind = 0

    def get_color(self, color_name):
        """
        Helper that fetches the appropriate color values from the renderer
        NOTE: get_color will FAIL if begin_rendering has not been called first
        """
        try:
            return getattr(self.renderer, color_name)()
        # TODO: Figure out which exceptions to catch here
        except AttributeError as e:
            raise AttributeError("%s -- Maybe begin_rendering hasn't been called yet?" % str(e))

    def draw_lines(self, lines):
        for line in lines:
            self.draw_actions.put(DrawAction(self.renderer.draw_line_3d, [line.start, line.end, line.color], {}))

    def draw_polyline(self, points, color=Colors.white):
        self.draw_actions.put(DrawAction(self.renderer.draw_polyline_3d, [points], {'color': color}))

    def draw_ball(self, ball_location_xyz, color=Colors.pink):
        r = consts.ball_radius_uu
        r2 = consts.sqrt2 * r / 2.0
        p = ball_location_xyz
        d0 = np.array([[0,0,r],[0, r2, r2],[0,r,0],[0,r2,-r2],[0,0,-r],[0,-r2,-r2],[0,-r,0],[0,-r2,r2],[0,0,r]])
        c0 = [p + d for d in d0]
        d1 = np.array([[0,0,r],[r2,0,r2],[r,0,0],[r2,0,-r2],[0,0,-r],[-r2,0,-r2],[-r,0,0],[-r2,0,r2],[0,0,r]])
        c1 = [p + d for d in d1]
        self.draw_polyline(c0, color)
        self.draw_polyline(c1, color)

    def draw_text(self, text, color=Colors.white, coords=None):
        if coords is not None:
            self.draw_actions.put(DrawAction(self.renderer.draw_string_3d,
                                             [coords, 1, 1, text],
                                             {'color': color}))
        else:
            self.draw_actions.put(DrawAction(self.renderer.draw_string_2d,
                                             [200, 700, 1, 1, text],
                                             {'color': color}))

    def draw_console_text(self, text, color=Colors.white):
        if self.console_buffer[self.next_console_ind].frame_age == 0:
            self.log.error("Putting too many messages into console! Limit is %d per frame" % _console_length_limit)
        self.console_buffer[self.next_console_ind] = ConsoleLine(text=text, color=color)
        self.next_console_ind += 1
        if self.next_console_ind == _console_length_limit:
            self.next_console_ind = 0


    def _build_color(self, kwargs):
        """ Colors must be built after begin_rendering is called because flatbuffer or something """
        if 'color' in kwargs:
            kwargs['color'] = self.get_color(kwargs['color'])

    def _render_console(self):
        console_fully_rendered = False
        console_y = 5
        current_ind = self.next_console_ind
        while not console_fully_rendered:
            line = self.console_buffer[current_ind]
            if line.frame_age < _console_age_limit:
                self.renderer.draw_string_2d(5, console_y, 1, 1, line.text, color=self.get_color(line.color))
                console_y += _text_height
                line.frame_age += 1
            current_ind += 1
            if current_ind == _console_length_limit:
                current_ind = 0
            console_fully_rendered = (current_ind == self.next_console_ind)


    def render_frame(self):
        self.renderer.begin_rendering()
        self._render_console()
        while not self.draw_actions.empty():
            action, args, kwargs = self.draw_actions.get()
            self._build_color(kwargs)
            action(*args, **kwargs)
            self.draw_actions.task_done()
        self.renderer.end_rendering()
