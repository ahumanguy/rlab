# Merges interfaces between rlutil classes and RLbot structs in order to provide a single entrypoint for my code

import logging
import numpy as np

import kinematics
import consts
import game_clock

import rlbot.utils.structures.game_data_struct as rlbot_structs

from RLUtilities import GameInfo
from RLUtilities.Simulation import Car, Ball


# ============= RLBOT TYPE CONVERSIONS ==============
def xyz_to_rlbot_vector(x, y, z):
    return rlbot_structs.Vector3(x, y, z)


def rlbot_vector_to_xyz(vec):
    return vec.x, vec.y, vec.z

# ============ SHIMS FOR RLBOT/RLUTIL ================
class PhysicsShim:
    @staticmethod
    def _to_kinematics_fields(physics: rlbot_structs.Physics):
        # Read the shim into the format expected for the kinematics class's init
        return (PhysicsShim.read_location_xyz_uu(physics),
                PhysicsShim.read_rpy_rads(physics),
                PhysicsShim.read_velocity_xyz_uu_s(physics))

    @staticmethod
    def read_rpy_rads(physics: rlbot_structs.Physics):
        rot = physics.rotation
        return rot.roll, rot.pitch, rot.yaw

    @staticmethod
    def read_location_xyz_uu(physics: rlbot_structs.Physics):
        return rlbot_vector_to_xyz(physics.location)

    @staticmethod
    def read_velocity_xyz_uu_s(physics: rlbot_structs.Physics):
        vel = physics.velocity
        return vel.x, vel.y, vel.z

# TODO: Examine whether BallShim is useful - rlutil ball predictions are encapsulated in the gameinfo class,
#       not the ball class, so most operations involving ball predictions will need gameinfo
class BallShim(kinematics.Ball, PhysicsShim):
    def __init__(self, ball_rlbot: rlbot_structs.BallInfo, ball_rlutil: Ball):
        kinematics.Ball.__init__(self,
                                 *self._to_kinematics_fields(ball_rlbot.physics))
        self._rlbot = ball_rlbot
        self._rlutil = ball_rlutil

class CarShim(kinematics.Kinematics, PhysicsShim):
    def __init__(self, car_rlbot: rlbot_structs.PlayerInfo, car_rlutil: Car):
        kinematics.Kinematics.__init__(self, *self._to_kinematics_fields(car_rlbot.physics))
        self._rlbot = car_rlbot
        self._rlutil = car_rlutil

    def get_height_uu(self):
        # TODO: Modify to return accurate height
        return consts.octane_com_height_uu

    def update(self, car_rlbot: rlbot_structs.PlayerInfo):
        self._rlbot = car_rlbot
        self.update_fields(*self._to_kinematics_fields(car_rlbot.physics))

# TODO: Implement a simple state here that stores the core useful values?
class GameInfoShim:
    ball_prediction_period_s = None

    def __init__(self):
        self.log = logging.getLogger(__name__ + ".GameState")
        self.this_player = None
        self.this_team = None
        self.rlbot = None
        self.rlutil = None
        self.ball_predictions = None
        game_clock.start_clock(self.log)
        self.clock = game_clock.get_clock()

    def initialize_info(self, player_index, team, field_info):
        self.this_player = player_index
        self.this_team = team
        self.rlutil = GameInfo.GameInfo(player_index, team, field_info)

    def _read_agent_state(self, agent):
        self.ball_predictions = agent.get_ball_prediction_struct()
        # Compute the value for how many seconds ahead ball prediction looks once - expected to be static
        if not self.ball_prediction_period_s:
            self.ball_prediction_period_s = consts.physics_slice_time_s * self.ball_predictions.num_slices

    def update(self, agent, packet: rlbot_structs.GameTickPacket):
        self.rlbot = packet
        self.rlutil.read_packet(packet)
        self._read_agent_state(agent)
        self.clock.update_time_s(self.get_game_time_s())

    def is_in_kickoff(self):
        return self.rlbot.game_info.is_kickoff_pause

    def get_ball_location_cartesian_uu(self, time_elapsed_s=0):
        if not 0 <= time_elapsed_s <= self.ball_prediction_period_s:
            raise ValueError("Elapsed time must be between 0.0s and 6.0s")
        if time_elapsed_s == 0:
            return rlbot_vector_to_xyz(self.rlbot.game_ball.physics.location)
        elif not self.ball_predictions:
            self.log.error("Can't fetch predictions for time %s because none available - returning current location" %
                           time_elapsed_s)
            return rlbot_vector_to_xyz(self.rlbot.game_ball.physics.location)

        # Find the closest match by brute forcing through the list
        target_game_time = self.get_game_time_s() + time_elapsed_s
        best_delta_time = self.ball_prediction_period_s
        closest_slice = None
        for slice in self.ball_predictions.slices:
            delta_time = np.abs(slice.game_seconds - target_game_time)
            if delta_time < best_delta_time:
                best_delta_time = delta_time
                closest_slice = slice
            elif delta_time > best_delta_time and closest_slice is not None:
                # Since time monotonically increases, when the delta increases, we've passed the match
                break
        if closest_slice is None:
            self.log.error("Couldn't find a prediction for game time %f" % target_game_time)
            return rlbot_vector_to_xyz(self.rlbot.game_ball.physics.location)
        else:
            return rlbot_vector_to_xyz(closest_slice.physics.location)

    def find_own_car(self, cars: [CarShim]):
        return cars[self.this_player]

    def get_game_time_s(self):
        return self.rlbot.game_info.seconds_elapsed

    def get_ball_prediction_slices(self):
        slice_index = 0
        while slice_index < self.ball_predictions.num_slices:
            yield self.ball_predictions.slices[slice_index]
            slice_index += 1
        raise StopIteration

    def get_ball_prediction_points(self):
        if self.ball_predictions is None:
            return None
        return [PhysicsShim.read_location_xyz_uu(slice.physics) for slice in self.ball_predictions.slices]