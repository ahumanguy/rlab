# Simple module for holding a single static instance of the game clock that gets updated by the
# game_info shim at the start of every tick.

def get_time_s():
    if GameClock._instance:
        return GameClock._instance.game_time_s
    else:
        GameClock._instance.logger.error("Attempted to get time from clock before initialized.")

def start_clock(logger):
    if GameClock._instance:
        raise RuntimeError("Tried to initialize static instance of game clock twice!")
    else:
        GameClock._instance = GameClock(logger)

def get_clock():
    return GameClock._instance

class GameClock:
    _instance = None

    def __init__(self, logger):
        self.game_time_s = 0
        self.logger = logger

    def update_time_s(self, new_time_s):
        if new_time_s < self.game_time_s:
            self.logger.warning("Updating clock with new time %f which is less than current time %f" %
                                (new_time_s, self.game_time_s))
        self.game_time_s = new_time_s