import kinematics

_zero_vector = kinematics._zero_vector

class EmptyKinematics(kinematics.Kinematics):
    """ simplifies constructing a trivial kinematics object to use for testing """
    def __init__(self):
        kinematics.Kinematics.__init__(self,
                                       location_xyz_uu=_zero_vector,
                                       rotation_rpy_rads=_zero_vector,
                                       velocity_xyz=_zero_vector,
                                       acceleration_xyz=_zero_vector)
