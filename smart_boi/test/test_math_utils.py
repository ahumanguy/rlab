import pytest
from pytest import approx

import numpy as np
import pdb

import math_utils
import kinematics
import consts

from .mock_classes import EmptyKinematics

# TODO: test_compute_inner_angles

def assert_vector_equality(v_expected, v_actual):
    x_expected, y_expected, z_expected = v_expected
    x_actual, y_actual, z_actual = v_actual
    assert x_expected == x_actual
    assert y_expected == y_actual
    assert z_expected == z_actual

def test_consts_acceleration():
    assert consts.max_throttle_accel_uu_s2 == consts.compute_throttle_acceleration_uu_s2(0, 1.0)
    assert -consts.max_throttle_accel_uu_s2 == consts.compute_throttle_acceleration_uu_s2(0, -1.0)
    assert 0.0 == consts.compute_throttle_acceleration_uu_s2(consts.max_throttle_speed_uu_s, 1.0)
    assert -consts.accel_brake_uu_s2 == consts.compute_throttle_acceleration_uu_s2(consts.max_throttle_speed_uu_s, -1.0)
    assert -consts.accel_coast_uu_s2 == consts.compute_throttle_acceleration_uu_s2(consts.max_throttle_speed_uu_s, 0.0)


def test_compute_vehicle_acceleration_at_rest():
    test_fn = math_utils.compute_vehicle_acceleration_xyz
    k = EmptyKinematics()
    # Vehicle at rest
    accel_expected_max = consts.max_throttle_accel_uu_s2 * consts.forward_vector
    assert_vector_equality(accel_expected_max, test_fn(k, 1.0))
    # Should get no acceleration because it's not moving
    accel_expected_coast = consts.zero_vector
    assert_vector_equality(accel_expected_coast, test_fn(k, 0.0))
    accel_expected_brake = -consts.max_throttle_accel_uu_s2 * consts.forward_vector
    assert_vector_equality(accel_expected_brake, test_fn(k, -1.0))

def test_compute_vehicle_acceleration_forwards():
    test_fn = math_utils.compute_vehicle_acceleration_xyz
    k = EmptyKinematics()
    k._velocity = consts.forward_vector * consts.max_throttle_speed_uu_s
    # Vehicle sliding sideways should be treated same as vehicle at rest when throttle is applied
    accel_expected_forwards = consts.zero_vector
    assert_vector_equality(accel_expected_forwards, test_fn(k, 1.0))
    # Should get no acceleration because it's "not moving"
    accel_expected_coast = -consts.accel_coast_uu_s2 * consts.forward_vector
    assert_vector_equality(accel_expected_coast, test_fn(k, 0.0))
    accel_expected_brake = -consts.accel_brake_uu_s2 * consts.forward_vector
    assert_vector_equality(accel_expected_brake, test_fn(k, -1.0))

def test_compute_vehicle_acceleration_sideways():
    test_fn = math_utils.compute_vehicle_acceleration_xyz
    k = EmptyKinematics()
    k._velocity = consts.right_vector * consts.max_throttle_speed_uu_s
    # Vehicle sliding sideways should be treated same as vehicle at rest when throttle is applied
    accel_expected_max = consts.max_throttle_accel_uu_s2 * consts.forward_vector
    assert_vector_equality(accel_expected_max, test_fn(k, 1.0))
    # Should get no acceleration because it's "not moving"
    accel_expected_coast = consts.zero_vector
    assert_vector_equality(accel_expected_coast, test_fn(k, 0.0))
    accel_expected_brake = -consts.max_throttle_accel_uu_s2 * consts.forward_vector
    assert_vector_equality(accel_expected_brake, test_fn(k, -1.0))

def test_euler_to_forward():
    forwards = consts.forward_vector
    simple_case = (0.0, 0.0)
    assert approx(forwards) == math_utils.euler_angles_to_forward_vector(*simple_case)
    backwards = consts.backward_vector
    upside_down_case = (np.pi, 0.0)
    assert approx(backwards) == math_utils.euler_angles_to_forward_vector(*upside_down_case)
    full_turn_case = (0.0, np.pi)
    assert approx(backwards) == math_utils.euler_angles_to_forward_vector(*full_turn_case)
    side_face = consts.left_vector
    quarter_turn_case = (0.0, -np.pi / 2.0)
    assert approx(side_face) == math_utils.euler_angles_to_forward_vector(*quarter_turn_case)

def test_fix_wraparound_rad():
    easy_rot = np.pi / 2
    # No-op
    assert easy_rot == math_utils.fix_wraparound_rad(easy_rot)
    too_far_positive = easy_rot + 4 * np.pi
    # Subtraction
    assert approx(easy_rot) == math_utils.fix_wraparound_rad(too_far_positive)
    too_far_negative = easy_rot - 4 * np.pi
    # Addition
    assert approx(easy_rot) == math_utils.fix_wraparound_rad(too_far_negative)
    zero_rot = 0
    # No-op
    assert zero_rot == math_utils.fix_wraparound_rad(zero_rot)
    neg_rot = -np.pi + 0.01
    assert neg_rot == math_utils.fix_wraparound_rad(neg_rot)
    way_neg_rot = neg_rot - 8 * np.pi
    assert approx(neg_rot) == math_utils.fix_wraparound_rad(way_neg_rot)

def test_heading_correction_rad():
    simple_case = ((0, 0), (1, 0), 0)
    assert approx(0) == math_utils.compute_heading_correction_rads(*simple_case)
    quarter_turn = ((0, 0), (0, -1), 0)
    assert approx(-np.pi / 2) == math_utils.compute_heading_correction_rads(*quarter_turn)
    eighth_turn = ((0, 0), (0.5, 0.5), 0)
    assert approx(np.pi / 4) == math_utils.compute_heading_correction_rads(*eighth_turn)

def test_inscribe_sphere():
    p1 = consts.forward_vector
    p2 = -consts.forward_vector
    sphere = math_utils.inscribe_sphere(p1, p2)
    assert_vector_equality(consts.zero_vector, sphere.center_xyz_uu)
    assert 1.0 == sphere.radius_uu
    p3 = consts.right_vector
    sphere = math_utils.inscribe_sphere(p1, p3)
    assert_vector_equality([0.5, 0.5, 0.0], sphere.center_xyz_uu)
    assert np.sqrt(0.5) == sphere.radius_uu

def test_integrate_kinematics_trivial():
    v = np.array([2.0, 0.0, 0.0], dtype=consts.float_dtype)
    a = np.array([1.0, 1.0, 0.0], dtype=consts.float_dtype)
    k = kinematics.Kinematics(consts.zero_vector, consts.zero_vector, v, a)
    k_actual = math_utils.integrate_kinematics(k, 1.0)
    v_expected = np.array([3.0, 1.0, 0.0], dtype=consts.float_dtype)
    a_expected = a
    p_expected = np.array([2.5, 0.5, 0.0], dtype=consts.float_dtype)
    assert all(p_expected == k_actual._position)
    assert all(v_expected == k_actual._velocity)
    assert all(a_expected == k_actual._acceleration)
    # Pycharm thinks one of these values is a bool...?
    #assert all(consts.zero_vector == k_actual._orientation)
