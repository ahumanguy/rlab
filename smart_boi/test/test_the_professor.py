import pytest
from pytest import approx

import numpy as np
import the_professor
import consts
from .mock_classes import EmptyKinematics

_zero_vector = consts.zero_vector

def test_exceptions():
    # compute_reachable_regions exceptions
    car_mock = EmptyKinematics()
    with pytest.raises(ValueError):
        regions = [r for r in the_professor.compute_reachable_regions(car_mock, 0.0)]

def test_compute_reachable_regions_trivial():
    time_window = consts.physics_slice_time_s
    car_mock = EmptyKinematics()
    regions = [r for r in the_professor.compute_reachable_regions(car_mock, time_window)]
    # Should only have computed for a single time step
    assert len(regions) == 1
    region = regions[0]
    assert approx(_zero_vector) == region.center_xyz_uu
    initial_acceleration = consts.compute_throttle_acceleration_uu_s2(0.0)
    expected_distance = 0.5 * initial_acceleration * pow(time_window, 2)
    assert approx(expected_distance) == region.radius_uu
    # Run again for two steps...
    regions = [r for r in the_professor.compute_reachable_regions(car_mock, time_window * 2.0)]
    assert 2 == len(regions)
    # Until the region computation gets more advanced, we'll only have one region
    region2 = regions[1]
    assert approx(_zero_vector) == region2.center_xyz_uu
    # Adding to expected distance from first test...
    velocity = initial_acceleration * time_window
    acceleration = consts.compute_throttle_acceleration_uu_s2(velocity)
    position = expected_distance + velocity * time_window + 0.5 * acceleration * pow(time_window, 2)
    assert approx(position) == region2.radius_uu

