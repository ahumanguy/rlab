# RLab

## Architecture and Design
### Tenets
* Never assume there is only one SmartBoi running in the kernel
* Code goes into the modules that best encapsulate the code's intent
* If no module is an obvious choice for a particular piece of code, re-think the code and the intent of the modules
* Everything that can be tested, should be tested
* Always start with the trivial case, but leave room to improve
* Look for standard python solutions for doing common things before implementing your own solution

### Module Intent
#### Agent
* Simple container - should contain minimal logic
* Constructs and intializes components
* Passes information between components when appropriate
* "Schedules" component execution

#### Brain
* Observes game state and returns desired state of the vehicle
* Encapsulates the "strategy" of the bot
* Planning based on specific AI approaches goes here - minimax, ML-trained, state machine, etc

#### The Professor
* Handles state modeling/prediction
* Provides utility functions for any components that need to predict an outcome
* physics-based planning functionality should go here (aiming, collision predictions, etc.)

#### Pilot (part of maneuvers shim)
* Handles execution of maneuvers
* Receives state requests from brain, passed via the agent, and attempts to path to state to meet constraints
* May extend this class to correct for intent of a given maneuver (i.e. use a pid loop to stabilize trajectory)

#### \_shim
* Abstraction layer between our bot and other RL* modules
* All imports and calls to other RL* modules or methods of their classes go in a shim 
* get_ functions return information stored in member classes
* read_ functions return information stored in the class which is passed in - they are static

#### \_util
* Static functions and utility classes for simple operations
* Keep imports in a util module to a minimum
* Meant to hold commonly used logic or simplify complicated computations