Acceleration and Friction (without handbrake):
https://www.youtube.com/watch?v=ueEmiDM94IE - 15:25

Ball Collision Physics:
https://www.youtube.com/watch?v=9uh8-nBlufM - 1:10

Dodge Physics:
https://www.youtube.com/watch?v=pX950bhGhJE - 3:20

Car Stats:
https://github.com/OfficialHalfwayDead/RocketLeaguePatchTesterResults/blob/master/current_patch/logs/CarStats